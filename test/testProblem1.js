const path = require("path");
const callback = require(path.join(__dirname, "../problem1"));

callback((err, success) => {
    if (err) {
        console.error("Error occured!");
        console.error("Errors: ", err);
        console.log("Some of the successful writes: ", success);
    } else {
        console.log("Successfully executed!");
        console.log(success);
    }
})