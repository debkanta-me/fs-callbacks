const fs = require("fs");
const path = require("path");

function callback(cb) {
    const unlinkErrors = [];
    const unlinks = [];
    let fileCount = 0;
    let unlinkExecutedCount = 0;

    fs.readFile(path.join(__dirname, "lipsum.txt"), "utf8", (err, lipsum) => {
        if (err) {
            cb(err);
        } else {
            fs.writeFile(path.join(__dirname, `newFile${fileCount += 1}.txt`), JSON.stringify(lipsum.toUpperCase()), (err) => {
                if (err) {
                    cb(err);
                } else {
                    fs.appendFile(path.join(__dirname, "filenames.txt"), `newFile${fileCount}.txt`, (err) => {
                        if (err) {
                            cb(err);
                        } else {
                            fs.readFile(path.join(__dirname, `newFile${fileCount}.txt`), 'utf8', (err, data) => {
                                if (err) {
                                    cb(err);
                                } else {
                                    const lowerCaseSentences = data.toLowerCase().split(".");

                                    fs.writeFile(path.join(__dirname, `newFile${fileCount += 1}.txt`), JSON.stringify(lowerCaseSentences), (err) => {
                                        if (err) {
                                            cb(err);
                                        } else {
                                            fs.appendFile(path.join(__dirname, "filenames.txt"), `\nnewFile${fileCount}.txt`, (err) => {
                                                if (err) {
                                                    cb(err);
                                                } else {
                                                    fs.readFile(path.join(__dirname, `newFile${fileCount}.txt`), 'utf8', (err, data) => {
                                                        if (err) {
                                                            cb(err);
                                                        } else {
                                                            try {
                                                                const writeData = JSON.parse(data).sort((a, b) => a > b ? 1 : -1);

                                                                fs.writeFile(path.join(__dirname, `newFile${fileCount += 1}.txt`), writeData, (err) => {
                                                                    if (err) {
                                                                        cb(err);
                                                                    } else {
                                                                        fs.appendFile(path.join(__dirname, "filenames.txt"), `\nnewFile${fileCount}.txt`, (err) => {
                                                                            if (err) {
                                                                                cb(err);
                                                                            } else {
                                                                                fs.readFile(path.join(__dirname, `filenames.txt`), 'utf8', (err, data) => {
                                                                                    if (err) {
                                                                                        cb(err);
                                                                                    } else {
                                                                                        const fileNames = data.split("\n");

                                                                                        fileNames.forEach((file) => {
                                                                                            fs.unlink(path.join(__dirname, file), (err) => {
                                                                                                unlinkExecutedCount += 1;

                                                                                                if (err) {
                                                                                                    unlinkErrors.push(err);
                                                                                                } else {
                                                                                                    unlinks.push(file);
                                                                                                }

                                                                                                if (unlinkErrors.length > 0 && fileNames.length == unlinkExecutedCount) {
                                                                                                    cb(unlinkErrors, unlinks);
                                                                                                } else if (fileNames.length == unlinkExecutedCount) {
                                                                                                    cb(null, unlinks);
                                                                                                }
                                                                                            })
                                                                                        })
                                                                                    }
                                                                                })
                                                                            }
                                                                        })
                                                                    }
                                                                })
                                                            } catch (e) {
                                                                cb(new Error("couldn't parse the JSON data!"));
                                                            }
                                                        }
                                                    })
                                                }
                                            })
                                        }
                                    })
                                }
                            })
                        }
                    })
                }
            })
        }
    })
}

module.exports = callback;