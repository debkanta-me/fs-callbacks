const fs = require("fs");
const path = require("path");

function callback(cb) {
    const errors = [];
    const successfulWrite = [];
    const unlinkErrors = [];
    let executedCount = 0;
    let unlinkCount = 0;
    let unlinkExecutionCount = 0;
    const files = Array.from(Array(10).keys());

    fs.mkdir(path.join(__dirname, "random"), (err) => {
        if (err) {
            cb(err, successfulWrite);
        } else {
            files.forEach((file) => {
                fs.writeFile(path.join(__dirname, `random/file${file}.json`), JSON.stringify({ ["name"]: "Debkanta!" }), (err) => {
                    executedCount += 1;

                    if (err) {
                        errors.push(err);
                    } else {
                        successfulWrite.push(`random/file${file}.json`);
                    }

                    if (errors.length > 0 && executedCount == files.length) {
                        cb(errors, successfulWrite);
                    } else if (executedCount == files.length) {
                        fs.readdir('random', (err, files) => {
                            files.forEach((file) => {
                                fs.unlink(path.join(__dirname, `random/${file}`), (err) => {
                                    unlinkExecutionCount += 1;

                                    if (err) {
                                        unlinkErrors.push(err);
                                    } else {
                                        unlinkCount += 1;
                                    }

                                    if (unlinkErrors > 0 && unlinkExecutionCount == files.length) {
                                        cb(unlinkErrors);
                                    } else if (unlinkExecutionCount == files.length) {
                                        cb(null, successfulWrite);
                                    }
                                });
                            })
                        })
                    }
                });
            })
        }
    })
}

module.exports = callback;